<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Westwerk</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="assets/min.css">
</head>

<body>
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#"><img src="assets/logo-white-2x.png"/></a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li><a href="#">Work</a></li>
                                <li class="active"><a href="#">About</a></li>
                                <li><a href="#">News</a></li>
                                <li><a href="#">Connect</a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="page-content">
    <div class="container con-full-width">
        <div class="row row-xs-height banner">
            <div class="col-xs-12 col-xs-height col-middle">
                <div class="banner-content-wrapper">
                    <div class="banner-content">
                        <h1>Westwerk is a digitally forward <br/>agency driven by smart design</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row row-min-height row-md-height row-sm-height row-xs-height">
            <div class="col-md-3 col-sm-0 col-xs-0 col-md-height col-sm-height col-xs-height"></div>
            <div class="col-md-6 col-sm-12 col-xs-12 col-middle col-md-height col-sm-height col-xs-height text-center">
                <h2>our philosophy</h2>
                <p>We think that, in work as in life, you get the best results when you keep it simple. For us, that's
                    meant hiring smart, talented people, doing great work and being honest with our clients.</p>
            </div>
            <div class="col-md-3 col-sm-0 col-xs-0 col-md-height col-sm-height col-xs-height"></div>
        </div>
    </div>
    <div class="container con-bsu con-full-width">
        <div class="row row-min-height row-sm-height">
            <div class="col-sm-6 col-sm-height col-bg"></div>
            <div class="col-sm-6 col-sm-height col-middle green-bg text-center col-client">
                <div class="client-wrapper">
                    <img src="assets/logo-boise-state-university.png"/>
                    <h3>Website Design + Website Development</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et libero nec metus efficitur
                        vehicula. Proin ex neque, euismod a tristique in, accumsan eu nibh. Duis ac tortor augue.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container con-rr con-full-width">
        <div class="row row-min-height row-sm-height">
            <div class="col-sm-6 col-sm-height col-middle green-bg text-center col-client">
                <div class="client-wrapper">
                    <img src="assets/logo-rush-river.png"/>
                    <h3>Website Design + Website Development</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et libero nec metus efficitur
                        vehicula. Proin ex neque.</p>
                </div>
            </div>
            <div class="col-sm-6 col-sm-height col-bg"></div>
        </div>
    </div>
    <div class="dark-bg">
        <div class="container">
            <div class="row row-min-height row-sm-height row-xs-height">
                <div class="col-sm-12 col-xs-12 col-sm-height col-xs-height col-middle">
                    <h1>We’re always looking for more <br/>good people.</h1>
                    <a href="#" class="btn">Join our team</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container con-accordion">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10 text-center">
                <h2>Our Values</h2>
                <div class="panel-group text-left" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    Hard Work
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingOne">
                            <div class="panel-body">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque dapibus tempor
                                lobortis. Sed fermentum dictum eros a congue. Nulla ut risus egestas neque tincidunt
                                egestas. Proin volutpat nec lorem ut porta. Praesent ipsum tortor, semper congue cursus
                                nec, blandit a purus. Fusce varius nunc ac diam imperdiet suscipit. In lobortis ex nec
                                augue lacinia, id porttitor quam efficitur. Nullam auctor scelerisque elementum. Proin
                                elit enim, feugiat non convallis nec, tincidunt in tellus.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Support our team
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingTwo">
                            <div class="panel-body">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque dapibus tempor
                                lobortis. Sed fermentum dictum eros a congue. Nulla ut risus egestas neque tincidunt
                                egestas. Proin volutpat nec lorem ut porta. Praesent ipsum tortor, semper congue cursus
                                nec, blandit a purus. Fusce varius nunc ac diam imperdiet suscipit. In lobortis ex nec
                                augue lacinia, id porttitor quam efficitur. Nullam auctor scelerisque elementum. Proin
                                elit enim, feugiat non convallis nec, tincidunt in tellus.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Build cool stuff
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingThree">
                            <div class="panel-body">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque dapibus tempor
                                lobortis. Sed fermentum dictum eros a congue. Nulla ut risus egestas neque tincidunt
                                egestas. Proin volutpat nec lorem ut porta. Praesent ipsum tortor, semper congue cursus
                                nec, blandit a purus. Fusce varius nunc ac diam imperdiet suscipit. In lobortis ex nec
                                augue lacinia, id porttitor quam efficitur. Nullam auctor scelerisque elementum. Proin
                                elit enim, feugiat non convallis nec, tincidunt in tellus.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
    <div class="container con-team con-full-width">
        <div class="row">
            <div class="col-md-12 team-header text-center">
                <h2>The Team</h2>
            </div>
        </div>
        <div class="row text-left row-employees">
            <?php
            $file = dirname(__FILE__) . '/Employees.json';
            if(file_exists($file)) {
                $json_encoded = file_get_contents($file, true);
                $data = json_decode($json_encoded, true);
                $counter = 0; ?>
                    <?php
                    foreach($data as $employee) { $counter++;
                        $fname = $employee['name']['first'];
                        $lname = $employee['name']['last'];
                        $position = $employee['position'];
                        $empImg = $employee['image'];
                        $empImgUrl = 'assets/'.$empImg;
                        echo '
                        <img src="'.$empImgUrl.'" onError="this.style.display=\'none\';this.className=\'noImg\';"/>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 col-emp employee" style="background: transparent url('.$empImgUrl.') no-repeat center center;" data-id="'.$counter.'">
                            <a class="emp-content-wrapper">
                                <div class="emp-content">
                                    <span class="name">' . $fname . ' ' . $lname . '</span><br/><span class="emp-position">' . $position . '</span>
                                </div>
                            </a>
                         </div>';
                        if($counter >= 8) {
                            break;
                        }
                    }
                    ?>
                <?php
            } else {
                echo $file . 'does not exist!';
            } ?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a class="btn load-more">Load More</a>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-logo">
                <img src="assets/logo-icon.png"/>
            </div>
            <div class="col-sm-6">
                <ul class="social">
                    <li>
                        <a href="">
                            <img src="assets/icon-facebook.png"/>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <img src="assets/icon-twitter.png"/>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <img src="assets/icon-linkedin.png"/>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <img src="assets/icon-dribble.png"/>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script src="assets/min.js"></script>
</body>
</html>
