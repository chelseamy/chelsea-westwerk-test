;(function($){
    jQuery(document).ready(function($) {
        function loadMoreEmployees(start) {
            $.ajax({
                type: "POST",
                url: "ajax.php",
                data : { start : start }
            }).done(function(result) {
                var employees = jQuery.parseJSON( result );
                // check if this is the final response
                if(employees[employees.length-1] == 'final') {
                    noMoreEmployees();
                    employees = employees.slice(0,-1);
                }
                // loop over each employee response and add them to UI
                $.each(employees, function(i, item) {
                    $('.row-employees').append(
                        '<img src="assets/'+employees[i].image+'" onError="this.style.display=\'none\';this.className=\'noImg\';"/>' +
                        '<div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 col-emp employee" style="background: transparent url(assets/'+employees[i].image+') no-repeat center center; display: none;" data-id="'+(parseInt(start) + i + 1)+'">' +
                            '<a class="emp-content-wrapper"> ' +
                                '<div class="emp-content">' +
                                    '<span class="name">'+employees[i].name.first+' '+employees[i].name.last+'</span><br/>' +
                                    '<span class="emp-position">'+employees[i].position+'</span>' +
                                '</div>' +
                            '</a>' +
                        '</div>'
                    );
                    $('.employee').fadeIn(500);
                });
            });
        }
        function noMoreEmployees() {
            $('.load-more').hide();
        }
        $('.load-more').click(function(){
            var start = $('.employee').last().attr('data-id');
            loadMoreEmployees(start);
        });
        
    });
})(jQuery);