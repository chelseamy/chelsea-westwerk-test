module.exports = function(grunt) {

    var jsFileList = [
        '_/js/partials/*.js'
    ];

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            dev: {
                options: {
                    style: 'expanded',
                    sourcemap: 'none'
                },
                files: {
                    '_/sass/main.scss': '_/sass/partials/_main.scss'
                }
            },
            dist: {
                options: {
                    style: 'compressed',
                    sourcemap: 'none'
                },
                files: {
                    'assets/min.css': '_/sass/main.scss'
                }
            }
        },
        concat: {
            options: {
                separator: ';\n'
            },
            dist: {
                src: [jsFileList],
                dest: 'assets/min.js'
            }
        },
        watch: {
            css: {
                files: '**/*.scss',
                tasks: ['sass']
            },
            js: {
                files: [
                    '_/js/**/*.js'
                ],
                tasks: ['concat']
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.registerTask('default',['watch']);
}