<?php
	$start = htmlspecialchars($_POST['start']);
	$response = array();
	$file = dirname(__FILE__) . '/Employees.json';
	if(file_exists($file)) {
		$json_encoded = file_get_contents($file, true);
		$data = json_decode($json_encoded, true);
		$data_to_return = array_slice($data, $start, 4);
		if(end($data_to_return) == end($data)) {
			array_push($data_to_return, 'final');
		}
		echo json_encode($data_to_return);
	}
?>